#Anastassia Bobokalonova
#April 3, 2017

require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.reduce(0) {|acc, el| acc + el}
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? {|string| string.include?(substring)}
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  all_letters = string.chars.uniq.sort
  all_letters.shift if all_letters[0] == " "
  letters = []

  all_letters.each do |ch|
    if string.count(ch) > 1
      letters << ch
    end
  end
  letters
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  words = string.split
  words.sort_by {|word| word.length}[-2..-1]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  missing = []
  ("a".."z").each do |letter|
    missing << letter if !string.include?(letter)
  end
  missing
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).reduce([]) do |years, year|
    if not_repeat_year?(year)
      years << year
    else
      years
    end
  end
end

def not_repeat_year?(year)
  year.to_s.chars == year.to_s.chars.uniq
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  wonders = []
  all_songs = songs.uniq

  all_songs.each do |song|
    wonders << song if no_repeats?(song, songs)
  end

  wonders
end

def no_repeats?(song_name, songs)
  (0...songs.length - 1).each do |i|
    if songs[i] == song_name
      return false if songs[i + 1] == song_name
    end
  end
  true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  remove_punctuation(string)
  return "" unless string.include?("c")

  c_words = string.split.select {|word| word.include?("c")}
  c_words.sort_by {|word| c_distance(word)}[0]
end

def c_distance(word)
  #Reverse the word to get the smallest positive index for c
  #More intuitive to sort
  word.reverse.index("c")
end

def remove_punctuation(string)
  string.delete!(".,!?;:")
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  ranges = []
  idx = 0
  while idx < arr.length - 1
    idx += 1
    if arr[idx] == arr[idx - 1]
      start_idx = idx - 1
      until arr[idx] != arr[start_idx]
        end_idx = idx
        idx += 1
      end
      ranges << [start_idx, end_idx]
    end
  end
  ranges
end

# Other Solution, more readable:
#   ranges = []
#   start_index = nil
#
#   arr.each_with_index do |el, idx|
#     next_el = arr[idx + 1]
#     if el == next_el
#       start_index = idx unless start_index
#     elsif start_index
#       ranges.push([start_index, idx])
#       start_index = nil
#     end
#   end
#   ranges
# end
